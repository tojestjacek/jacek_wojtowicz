# README #

Szanowni Państwo,

Załączam zadanie rekrutacyjne numer 1.
Zaimplementowałem rozwiązanie w języku Python.

W repozytorium załączone są następujące pliki:
-Node.py - klasa reprezentująca pojedynczy węzeł
-Tree.py - klasa reprezentująca całą strukturę,
zawiera metody wyliczające wymagane wartości
-main.py - konstruuje strukturę podaną w przykładzie
oraz wylicza dla niej podane wartości
-Tree_tests.py - przeprowadza testy jednostkowe dla metod
klasy Tree

Z poważaniem,
Jacek Wójtowicz