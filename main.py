from Tree import Tree
from Node import Node

#konstrukcja drzewa
tree=Tree(5)
tree._root._left=Node(3)
tree._root._left._left=Node(2)
tree._root._left._right=Node(5)
tree._root._right=Node(7)
tree._root._right._left=Node(1)
tree._root._right._right=Node(0)
tree._root._right._right._left=Node(2)
tree._root._right._right._right=Node(8)
tree._root._right._right._right._right=Node(5)


#kolejne wyniki
tree.Sum(tree.getRoot())
tree.Average(tree.getRoot())
tree.Median(tree.getRoot())