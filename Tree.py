from typing import List
from Node import Node

class Tree:
    def __init__(self,value: float)->None:
        self._root = Node(value)
        self._list = None

    def getRoot(self)->Node:
        return self._root

    def Sum(self,node: Node)->float:
        if node is None:
            return 0
        empty_list = []
        self.list_of_values(node, empty_list)
        return sum(self._list)

    def Average(self,node: Node)->float:
        if node is None:
            return 0
        empty_list = []
        self.list_of_values(node,empty_list)
        return  sum(self._list)/len(self._list)

    def Median(self,node: Node)->float:
        if node is None:
            return 0
        empty_list = []
        self.list_of_values(node,empty_list)
        self._list.sort()
        length=len(self._list)
        return self._list[int(length/2)] if length%2==1 else (self._list[int(length/2)-1]+self._list[int(length/2)])/2


    def list_of_values(self,node:Node,list: List=[])->List:
        if node is None:
            self._list=list
            return list
        list.append(node.getValue())
        self.list_of_values(node.getLeft(),list)
        self.list_of_values(node.getRight(),list)