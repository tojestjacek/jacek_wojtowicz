import unittest
from Tree import Tree
from Node import Node

class Test_Tree(unittest.TestCase):
    def test_normal_case_sum(self):
        test_tree=Tree(2)
        test_tree._root._right=Node(3)
        test_tree._root._left=Node(4)
        test_tree._root._left._left=Node(2)
        self.assertEqual(test_tree.Sum(test_tree.getRoot()),11)

    def test_normal_case_avarage(self):
        test_tree = Tree(1)
        test_tree._root._right = Node(2)
        test_tree._root._left = Node(4)
        test_tree._root._left._left = Node(5)
        self.assertEqual(test_tree.Average(test_tree.getRoot()), 3)

    def test_normal_case_even_median(self):
        test_tree = Tree(6)
        test_tree._root._right = Node(2)
        test_tree._root._left = Node(20)
        test_tree._root._left._left = Node(0)
        self.assertEqual(test_tree.Median(test_tree.getRoot()), 4)

    def test_normal_case_odd_median(self):
        test_tree = Tree(6)
        test_tree._root._right = Node(2)
        test_tree._root._right._right = Node(1)
        test_tree._root._left = Node(20)
        test_tree._root._left._left = Node(0)
        self.assertEqual(test_tree.Median(test_tree.getRoot()), 2)

    def test_unusual_case_one_element_sum(self):
        test_tree=Tree(4)
        self.assertEqual(test_tree.Sum(test_tree.getRoot()),4)

    def test_unusual_case_one_element_avarage(self):
        test_tree = Tree(2)
        self.assertEqual(test_tree.Average(test_tree.getRoot()), 2)

    def test_unusual_case_one_element_median(self):
        test_tree = Tree(6)
        self.assertEqual(test_tree.Median(test_tree.getRoot()), 6)

    def test_unusual_case_wrong_node_sum(self):
        test_tree = Tree(2)
        test_tree._root._left = Node(5)
        test_tree._root._right = Node(8)
        test_tree._root._right._right = Node(4)
        self.assertEqual(test_tree.Sum(test_tree.getRoot().getLeft().getLeft()), 0)

    def test_unusual_case_wrong_node_avarage(self):
        test_tree = Tree(2)
        test_tree._root._left = Node(5)
        test_tree._root._right = Node(8)
        test_tree._root._right._right = Node(4)
        self.assertEqual(test_tree.Average(test_tree.getRoot().getLeft().getLeft()), 0)

    def test_unusual_case_wrong_node_median(self):
        test_tree = Tree(2)
        test_tree._root._left = Node(5)
        test_tree._root._right = Node(8)
        test_tree._root._right._right = Node(4)
        self.assertEqual(test_tree.Median(test_tree.getRoot().getLeft().getLeft()), 0)

if __name__ == '__main__':
    unittest.main()