import Node

class Node:
    def __init__(self, value: float)->None:
        self._left = None
        self._right = None
        self._value = value

    def getLeft(self)->Node:
        return self._left

    def getRight(self)->Node:
        return self._right

    def getValue(self)->float:
        return self._value